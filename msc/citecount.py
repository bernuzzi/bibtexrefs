#!/usr/bin/env python
# Parse TeX file and count citations

import sys, re
from collections import Counter, OrderedDict
 
if __name__ == "__main__" :

    if len(sys.argv) < 1:
        print("Usage: python "+sys.argv[0]+" <fname>.tex")
        exit()
        
    tex = sys.argv[1]

    # Get ref list from bbl
    #ref = [re.findall(r'\\bibitem\{(.*)\}',line) for line in open(bbl)]
    #ref = list(filter(None, ref))
    #ref = [r[0] for r in ref]
    #print(ref)
    
    # Parse tex for cites
    cite = [re.findall(r'\\cite\{(.*?)\}',line) for line in open(tex)]
    cite = list(filter(None, cite))
    cite = [c[0] for c in cite]
    cite = [c.strip(' ') for c in cite]
    cite = list(filter(None, cite))
    cite = [c.strip(' ') for c in cite]
    #print(cite)
    ref = []
    for c in cite:
        for e in c.split(','):
            ref.append(e)
    #print(ref)

    # Count
    x = Counter(ref).most_common()
    for r in x: print(r)

