#!/bin/bash
# Clone/pull this repo and link bib file into TeX folder

HERE=$(pwd)

usage () {
    echo "Usage: $(basename "$0") [OPTIONS] [NAME]"
    echo " -h  help"
    echo " -d  dry-run (to nothing, display commands)"
    echo " -p  path where to clone the bibliography repo (pull if exists)"
    echo " -f  specify folder for symlink (created if not there)"
    echo " -a  symlink in all subfolders of ./"
}

symlink_all_subfolders () {
    for d in `ls -d */`
    do
	$debug ln -s $BIBPATH/bibtexrefs/refs.bib $d/references.bib
    done
}

# defaults
debug=
BIBPATH=$HOME
destfolder=false
DEST=
symlinkall=false

# https://wiki.bash-hackers.org/howto/getopts_tutorial
options=':hdp:f:a'
while getopts $options opt; do
  case "$opt" in
      h)
	  usage && exit 1
	  ;;
      d)
	  debug=echo
	  echo "-------------------------------------------"
	  echo "DRY-RUN: NOTHING WILL BE DONE!"
	  echo "-------------------------------------------"
	  ;;
      a)
	  symlinkall=true
	  ;;
      p)
	  BIBPATH=$OPTARG
	  ;;
      f)
	  destfolder=true
	  DEST=$OPTARG
	  ;;
      :)
	  echo "Option -$OPTARG requires an argument." >&2
	  usage >&2 && exit 1
	  ;;
      \?)
	  echo "Invalid option: -$OPTARG" >&2
	  usage >&2 && exit 1
	  ;;
  esac
done
shift $((OPTIND-1))

# clone or pull bib repo
if [ -d "$BIBPATH/bibtexrefs" ]; then
    $debug cd $BIBPATH/bibtexrefs
    $debug git pull
else
    $debug cd $BIBPATH
    $debug git clone git@bitbucket.org:bernuzzi/bibtexrefs.git
fi

cd $HERE

# symlink to all existing folders
if $symlinkall ; then
    symlink_all_subfolders && exit 1
fi

# symlink to given folder
if $destfolder ; then
    $debug mkdir -p $DEST
    $debug ln -s $BIBPATH/bibtexrefs/refs.bib $DEST/references.bib
    exit 1
else
    echo "destination folder not specified (use -f <DEST>) " && exit 0
fi
