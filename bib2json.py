#!/usr/bin/env python
# Parse a bibtex document into a json document
# Uses bibtexparser (sudo apt-get install bibtexparser)
# https://bibtexparser.readthedocs.io/en/master/index.html
# Sort rely on special bib key "date"

import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter
from operator import itemgetter # sorted 
##import json
import sys, re

def remove_brackets(strdata):
    return re.sub("{|}","",strdata)

def remove_newline(strdata):
    return re.sub("\n"," ",strdata)

def clean_str(strdata):
    strdata = remove_brackets(strdata)
    strdata = remove_newline(strdata)
    return strdata

def check_key(list_of_dict, key, pr=1):
    check = False
    for l in list_of_dict:
        if key not in l.keys():
            if pr:
                print("MISSING key = "+key)
                print(l)
            check = True
    return check

def print_entry(b,last=False):
    print('{')
    print('"title": "'+clean_str(b['title'])+'",')
    print('"authors": "'+clean_str(b['author'])+'",')
    print('"journal": "'+clean_str(b['journal'])+', '+b['volume']+', '+b['pages']+' ('+b['year']+')",')
    if not "doi" in b:
        print(b)
    print('"doi": "'+b['doi']+'",')
    if "eprint" in b:
        print('"eprint": "'+'arXiv:'+b['eprint']+'"')
    else:
        print('"eprint": ""')
    if last:
        print('}')
        print(']}')
    else:
        print('},')


if __name__ == "__main__" :

    if len(sys.argv) < 2:
        print("Usage: python "+sys.argv[0]+" <fname>.bib")
        exit()
        
    bibfilename = sys.argv[1]

    # Parser options
    #parser = BibTexParser()
    #parser.ignore_nonstandard_types = False
    #parser.homogenize_fields = True
    #parser.common_strings = True

    # Load the bib data
    with open(bibfilename) as bibtex_file:
        bibdata = bibtexparser.load(bibtex_file) #, parser)

    # Check presence of mandatory fields
    fields = ["date","author","title","journal","volume","pages","year"]
    for k in fields:
        check_key(bibdata.entries, k)

    # Sort by date
    sort_key = "date" 
    ##sort_key = "year" 
    bibdata_sorted = sorted(bibdata.entries, key=itemgetter(sort_key), reverse=True)
    # Note here python 3.6 native sorting should be the quickest option...
    
    # Dump to JSON (quick&dirty solution)
    print('{"refs":[')
    for b in bibdata_sorted[:-1]:
        print_entry(b)
    print_entry(bibdata_sorted[-1],last=True)

    # Dump to JSON
    # TODO: select field and clean strings
    # with open('result.json', 'w') as f:
    #     f.write(json.dumps(bibdata_sorted, sort_keys=True))

    #TODO Sort and dump should/could be done using bibtexparser writer ...
    #
    #writer = BibTexWriter()
    #writer.contents = fields # ['comments', 'entries']
    #writer.indent = '  '
    #writer.order_entries_by = ('author', 'year')
    #bibtex_str = bibtexparser.dumps(bib_database, writer) # this gives .bib but we want .json



