#!/usr/bin/env python
# Parse a bibtex document into a json document
# rely on pybtex (http://pybtex.sourceforge.net/)

import re
import sys
import pybtex.core
import pybtex.database
import pybtex.database.input.bibtex
import pprint
import json
import latex2unicode

class BibtexParser(object):
    def __init__(self, dictionaryFile="latex2unicode.json"):
        self.l2u = latex2unicode.Latex2Unicode(dictionaryFile)
        self.parser = pybtex.database.input.bibtex.Parser()

    def _remove_brackets(self, data):
        return re.sub("{|}","",data)
    
    def _string_handler(self, stuff):
        return self._remove_brackets(self.l2u.translate(unicode(stuff)))
    
    def _person_handler(self, person):
        return self._string_handler(unicode(person))
        
    def _entry_handler(self, entry):
        simpleEntry = {}
        
        simpleEntry['type'] = self._string_handler(entry.type)
        
        simpleEntry['key'] = self._string_handler(entry.key)
        
        for key, value in entry.fields.items():
            simpleEntry[key] = self._string_handler(value)
        
        for role, persons in entry.persons.items():
            simpleEntry[role] = []
            for person in persons:
                simpleEntry[role].append(self._person_handler(person))
                
        return simpleEntry
            
    def _bibliography_handler(self, bibliography):
        simpleBib = []
        for id, entry in bibliography.entries.items():
            simpleBib.append(self._entry_handler(entry))
        return simpleBib
    
    def parse(self,bibtexfile):
        bib = self.parser.parse_file(bibtexfile)
        return self._bibliography_handler(bib) 
    
def main() :
    if (len(sys.argv) < 2):
        print "usage: python " + sys.argv[0] + " path/to/bibtexfile.bib"
    else: 
        parser = BibtexParser()
        if sys.stdout.isatty():
            sys.stdout.write(json.dumps(parser.parse(sys.argv[1]), sort_keys=True, indent=4, ensure_ascii=False))
        else:
            sys.stdout.write(json.dumps(parser.parse(sys.argv[1]), sort_keys=True, indent=4, ensure_ascii=False).encode("utf-8"))
        sys.stdout.flush()
    
if __name__ == "__main__" :
    main()