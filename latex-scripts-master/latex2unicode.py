#!/usr/bin/env python
# Simple script translating simple latex symbols (e.g. \'{e} or \beta to unicode).

import re
import json
import fileinput
import sys

class Latex2Unicode(object):
    def __init__(self, dictionaryFile="latex2unicode.json"):
        self.loadDictionary(dictionaryFile)
        
    def translate(self, data):
        data = unicode(data)
        def _translate_one_match(match):
            return self.dictionary[match.group(0)]
        return self.regularExpression.sub(_translate_one_match, data)
    
    def loadDictionary(self, file = "latex2unicode.json"):
        f = open(file,'r')
        self.dictionary = json.loads(f.read())
        self.addBracketVariations()
        self.regularExpression = re.compile('|'.join(map(re.escape, self.dictionary)))
    
    def saveDictionary(self, file = "latex2unicode.json"):
        f = open(file,'w')
        f.write(json.dumps(self.dictionary, sort_keys=True, indent=4, ensure_ascii=True))
    
    # Add variations for the bracketted stuffs, e.g. from \'{e}, {\'e} is added
    def addBracketVariations(self):
        brackettedCharacter = re.compile("\\\\(.)\{(.+)\}")
        for k, v in self.dictionary.items():
            m = brackettedCharacter.match(k)
            if m:
                newK = "{\\" + m.group(1) + m.group(2) + "}"
                self.dictionary[newK] = v
        
    
def main():
    l2u = Latex2Unicode();
    output = u''
    for line in fileinput.input():
        output = output + l2u.translate(line)
    if sys.stdout.isatty():
        sys.stdout.write(output)
    else:
        sys.stdout.write(output.encode("utf-8"))
    sys.stdout.flush()

if __name__ == "__main__" :
    main()
    