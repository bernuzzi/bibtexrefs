#!/usr/bin/env python
# Simple script formatting a json bibliography in markdown.

import json
import fileinput
import sys
import textwrap

def formatTitle(title, level):
    titleFormatter = [
        "# {0} #\n\n",
        "## {0} ##\n\n",
        "### {0} ###\n\n",
        "#### {0} ####\n\n"
    ]
    
    if level < len(titleFormatter):
        return titleFormatter[level].format(title)
    else:
        return "** ERROR **"


def formatBibliographyByYear(bibliography, level):
    mdOutput = u''
    yearDictionnary = {}
    for entry in bibliography:
        year = "?"
        if 'year' in entry:
            year = entry.pop('year')
        if not year in yearDictionnary:
            yearDictionnary[year] = []
        yearDictionnary[year].append(entry)
    
    for year in sorted(yearDictionnary.keys()):
        mdOutput += formatTitle(year,level);
        for entry in yearDictionnary[year]:
            mdOutput += formatEntry(entry,level+1)
    
    return mdOutput

def formatEntry(entry, level):
    mdOutput = u''
    if 'title' in entry:
        mdOutput += formatTitle(entry['title'],level)

    if 'author' in entry:
        for i, author in enumerate(entry['author']):
            mdOutput += author
            if i < len(entry['author']) - 2:
                mdOutput += "; "
            elif i == len(entry['author']) - 2:
                mdOutput += " and "
            
    if 'type' in entry:
        if entry['type'] == "phdthesis":
            if 'school' in entry:
                if 'author' in entry:
                    mdOutput += " - "
                mdOutput += "**Phd thesis from " + entry['school']+ "**"
        elif entry['type'] == "article":
            if 'journal' in entry:
                if 'author' in entry:
                    mdOutput += " - "
                mdOutput += "**in " + entry['journal']+ "**"
        elif entry['type'] == "inproceedings":
            if 'booktitle' in entry:
                if 'author' in entry:
                    mdOutput += " - "
                mdOutput += "**in proceedings of " + entry['booktitle']+ "**"
    
    if 'url' in entry:
        if entry.has_key('author') or entry.has_key('type'):
            mdOutput += " - "
        mdOutput += "*[direct link](" + entry['url'] + ")*"
    
    mdOutput += "\n\n"
        
    if 'abstract' in entry:
        mdOutput += formatTitle("*Abstract*",level + 1)
        for line in textwrap.wrap(entry['abstract'], 100):
            mdOutput += "> " + line + "\n"
        mdOutput += "\n"
        
    if 'annote' in entry:
        mdOutput += formatTitle("*Comments*",level + 1)
        mdOutput += entry['annote'] + "\n"
        mdOutput += "\n"
            
    return mdOutput

def main():
    jsonInput = ""
    for line in fileinput.input():
        jsonInput += line
        
    bib = json.loads(jsonInput)
    
    mdOutput = formatBibliographyByYear(bib,0)
    
    if sys.stdout.isatty():
        sys.stdout.write(mdOutput)
    else:
        sys.stdout.write(mdOutput.encode("utf-8"))
    sys.stdout.flush()

if __name__ == "__main__" :
    main()