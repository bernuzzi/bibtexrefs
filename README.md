# BibTeX files

## refs

`refs.bib` is the main `bib` file, please keep it updated and try to use it as much as possible.

 * Entries are mostly taken from [HEP-SPIRES](https://labs.inspirehep.net/), [NASA-ADS](https://ui.adsabs.harvard.edu/), or journal servers. 
 * Entries are ordered alphabetically (first author) and for each author chrono-inverse. **PLEASE KEEP THIS ORDER**. 
 * The file will be periodically checked for double entries and typos, re-formatted using bibtool, and pushed. Note this is suboptimal for version control so should be done with care (and hopefully improved)

We want to keep HEP-SPIRES format. In the other cases, please, remove the original key and substitute it with a new one with the first author name,  e.g. change
```
 @article{1977ApJ...211..361L,
```
to
```
 @article{Liang:1977a,
      ads_key = "1977ApJ...211..361L",
```

## grouprefs

`grouprefs.bib` collects peer-reviewed references of the group.

 * include only peer-review published papers, possibly by [HEP-SPIRES](https://labs.inspirehep.net/)
 * keep chronological order
 * carefully check formatting (remove nonstadard chars, add the 'date' field etc)

## Tools

### Python scripts

`bib2json.py` is a homade Python script to generate a simple JSON file from a `*.bib` file using pybtex.
It is mostly mostly used with `grouprefs.bib`. 

A more sophisticated set of scripts from Cloderic Mars are backupped [here](latex-scripts-master) (previously available at `https://github.com/cloderic/latex-scripts`).

### Bibtool

[bibtool documentation](http://www.gerd-neugebauer.de/software/TeX/BibTool/en/)

Example: Check for double entries and auto formatting

```
$ echo "pass.comments = on" > bibtool_resource_file 
$ echo "preserve.key.case = on" >> bibtool_resource_file
$ echo ``print.line.length = 80'' >> bibtool_resource_file
$ bibtool -r bibtool_resource_file -d -i refs.bib 
```

### Bibstyles

For most common ones see e.g.

```
https://arxiv.org/help/hypertex/bibstyles
```

### Paper package

[This repo](https://bitbucket.org/bernuzzi/paperpackage/src/master/)
contains a simple python script to produce a submission package
(tarball) for scientific papers.
